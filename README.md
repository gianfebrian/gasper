#### GASPER ####

Gasper is JasperStarter Command wrapper built with Node.Js.

##### Some basic ideas of this project #####
Dos:
1. Provide easy to use tool to access jasperstarter cli
2. Centralized event log
3. Custom event log (seemlessly integration with your app logging mechanism)
4. Build command option with ease
5. Hide report generation from your app

Don'ts:
1. Report generation management (I will build another project for this one)
2. Separated reporting service (It is just another node library)

##### TODOs #####
1. Better EventEmitter implementation for centralizing event log
2. Script to fetch JasperStarter library
3. Script to fetch JDBC library
3. Unit testing coverage
4. Integration test (To see how this library interact with jasperstarter cli)
5. Optimization when data source type is List/collection / file
6. Interact with report params and filters
7. CI pipeline configuration
8. Test to solve real problems

##### Contribute #####
Currently, this project is use latest lts version (6.10.0). This project only use native node feature.
I have no intention to use transpiler. You may want to check whether you can use your intended feature by visiting [node.green](http://node.green/).

And please follow general git contribution guide and test your code and its coverage first.

