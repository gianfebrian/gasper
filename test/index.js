const chai = require('chai');

const assert = chai.assert;

global.chai = chai;
global.assert = assert;

describe('Gasper Unit Tests', function testCases() {
  require('./unit/utility/event');
  require('./unit/utility/logger');
  require('./unit/command/CommandBuilder');
  require('./unit/command/Command');
  require('./unit/Compile');
  require('./unit/Process');
});
