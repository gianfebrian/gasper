const Command = require('./Command');
const CommandBuilder = require('./CommandBuilder');

/**
 * @namespace command
 */

module.exports = { Command, CommandBuilder };
