const _ = require('lodash');

/**
 * This class is for building command chain
 * @memberof command
 */
class CommandBuilder {
  /**
   * Create new command builder
   * @param  {string} command - First and main command name (usually the binary name/path).
   * @param  {number} options.maxSubCommand - Maximum number sub command chaining
   */
  constructor(command, { maxSubCommand = 1 }) {
    this.command = command;
    this.maxSubCommand = maxSubCommand;
    this.args = [];
    this.output = '';
    this.subCommandCounter = 0;
  }

  /**
   * Set command option
   * @param  {string} optionName - Command option flag or name, e.g (-lah on ls)
   * @param  {any} value - Command option value.
   * @return {CommandBuilder} self.
   */
  option(optionName, value) {
    this.args.push(optionName, value);

    return this;
  }

  /**
   * Build an output of previously set options. This will also reset option chains.
   * @return {CommandBuilder} self.
   */
  buildOptions() {
    let commandOptions = '';

    _.each(this.args, (arg) => {
      if (_.isEmpty(arg)) {
        return commandOptions;
      }

      commandOptions += arg.join(' ');

      return commandOptions;
    });

    this.args = [];

    this.output += [this.command, commandOptions].join(' ');

    return this;
  }

  /**
   * Add sub command into chained command.
   * This method will cause previously build options to be built
   * and previous chained options will be reset.
   * @param  {string} subCommand - Sub command name.
   * @return {CommandBuilder} self.
   */
  subCommand(subCommand) {
    this.subCommandCounter += 1;

    if (this.subCommandCounter > this.maxSubCommand) {
      throw new Error('Maximum number of allowed sub command exceeded');
    }

    this.buildOptions();

    this.command = subCommand;

    return this;
  }

  /**
   * Get output command and its options string
   * @return {string} Command and its options.
   */
  output() {
    this.buildOptions();

    return this.output;
  }
}

module.exports = CommandBuilder;
