const _ = require('lodash');
const uuid = require('uuid');
const shell = require('shelljs');

const { event } = require('./../utility/event');
const CommandBuilder = require('./CommandBuilder');

const DEFAULT_BIN_PATH = process.env.DEFAULT_BIN_PATH;


/**
 * This class uses CommandBuilder interface and will centrelize event log.
 * @memberof command
 */
class Command extends CommandBuilder {
  /**
   * Create command chain interface
   * @param  {string} options.jasperBinaryPath - JasperStarter binary path.
   */
  constructor({ jasperBinaryPath }) {
    super(jasperBinaryPath || DEFAULT_BIN_PATH);
    this.maxSubCommand = 1;

    this.uuid = uuid.v4();
    this.delta = new Date();
    this.buildArgs();
  }

  /**
   * Calculate delta execution time.
   * @return {number} duration in miliseconds.
   */
  duration() {
    if (_.isEmpty(this.delta)) {
      this.delta = new Date();

      return null;
    }

    const duration = new Date() - this.delta;

    return duration;
  }

  /**
   * Interface to call compile sub command.
   * This will build previous set option and reset option chain.
   * @return {Command} self.
   */
  compile() {
    this.subCommand('compile');

    return this;
  }

  /**
   * Interface to call process sub command.
   * This will build previous set option and reset option chain.
   * @return {Command} self.
   */
  process() {
    this.subCommand('process');

    return this;
  }

  /**
   * Interface to exec command with its chained options. This will emit several general events:
   * <pre>executing, error, completed</pre>
   * In this context, will also emit several alias events:
   * <pre>
   * executing will also be mapped to compiling and processing
   * completed will also be mapped to compiled and processed
   * </pre>
   * Only completed (and its aliases) and error will have exec duration meta information.
   * @return {Promise} stdout / stderr.
   */
  exec() {
    const command = this.command;
    const output = this.output();

    event.log({ event: 'executing', message: output, meta: { command, output } });

    return new Promise((resolve, reject) => {
      shell.exec(output, (code, stdout, stderr) => {
        if (code !== 0) {
          event.log({
            level: 'error',
            event: 'error',
            message: stderr,
            meta: { command, output, duration: this.duration },
          });
          return reject(new Error(stderr));
        }

        event.log({
          event: 'completed',
          message: stdout,
          meta: { command, output, duration: this.duration },
        });

        return resolve(stdout);
      });
    });
  }
}

module.exports = Command;
