const { Command } = require('./command');

/**
 * Class for building command option chains to compile one report or all reports in a directory.
 * (convert .jrxml design file into .jasper).
 */
class Compile {
  /**
   * Create a compile command option chains.
   * @param  {object}  options - Command builder options.
   * @return {Compile} self constructor.
   */
  constructor(options) {
    this.command = new Command(options).compile();
  }

  /**
   * Set input option.
   * @param  {string}  value - Input file ('.jrxml') or directory.
   * @return {Compile} self.
   */
  input(value) {
    this.command.options(value);

    return this;
  }

  /**
   * Set output option.
   * @param  {string}  value - Directory or basename of outputfile(s).
   * @return {Compile} self.
   */
  output(value) {
    this.command.options('-o', value);

    return this;
  }

  /**
   * Exec compilation with chained options.
   * @return {string} stdout
   */
  exec() {
    return this.command.exec();
  }
}

module.exports = Compile;
