const _ = require('lodash');
const winston = require('winston');

/**
 * @module  logger
 */

/**
 * This class will configure winston log specific for gasper.
 */
class Configurator {
  /**
   * Create new configurator
   * @param  {object} options - Configurator options.
   */
  constructor(options) {
    this.logger = null;
    this.options = options;
    this.transportOptions = {};
    this.transports = [];
  }

  /**
   * Create or recreate logger instance (if options or transporst is modified).
   * @param  {object} loggerInstance - Logger instance to be used instead of default instance.
   * @return {object} Logger instance.
   */
  createOrRecreateLogger(loggerInstance) {
    if (!_.isEmpty(loggerInstance)) {
      this.logger = loggerInstance;

      return this.logger;
    }

    if (_.isEmpty(this.transports)) {
      this.transports.push(new winston.transports.Console(this.transport.options));
    }

    if (_.isEmpty(this.options)) {
      this.options = {
        transports: this.transports,
        exitOnError: false,
      };
    }

    this.logger = new winston.Logger(this.options);

    return this.logger;
  }

  /**
   * Add transport to logger. Defaults to winston.transports.Console
   * @param {object} transport - Transport instance or class.
   */
  addTransport(transport) {
    if (_.isEmpty(transport)) {
      throw new Error('Invalid winston transport');
    }

    if (!_.isFunction(transport.log)) {
      const Transport = transport;
      const transportInstance = new Transport(this.options);

      if (!_.isFunction(transportInstance.log)) {
        throw new Error('Invalid winston transport');
      }

      this.transports.push(transportInstance);
    } else {
      this.transports.push(transport);
    }

    this.createOrRecreateLogger();
  }

  /**
   * Set configurator options
   * @param {object} options - Configurator options.
   */
  setConfiguratorOptions(options) {
    if (_.isEmpty(options)) {
      return null;
    }

    this.options = options;
    this.createOrRecreateLogger();

    return this.options;
  }

  /**
   * Set transport options for all transports.
   * @param {object} options - Transport options.
   */
  setTransportOptions(options) {
    if (_.isEmpty(options)) {
      return null;
    }

    this.transportOptions = options;
    this.createOrRecreateLogger();

    return this.transportOptions;
  }
}

/**
 * Reusable static configuratorInstance
 * @type {Configurator}
 */
const configuratorInstance = new Configurator();

/**
 * Reusable static logger
 * @type {object}
 */
const logger = configuratorInstance.logger;

module.exports = { Configurator, configuratorInstance, logger };
