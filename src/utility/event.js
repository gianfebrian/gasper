const _ = require('lodash');
const { EventEmitter } = require('events');

const { logger } = require('./logger');

/**
 * @module  event
 */

/**
 * This class is for centralized event logging.
 */
class Event extends EventEmitter {
  /**
   * Create new event log.
   */
  constructor() {
    super();
    this.eventNames = ['info', 'warn', 'debug', 'error'];
  }

  /**
   * Event names getter.
   * @return {array} Event names.
   */
  get eventNames() {
    return this.eventNames();
  }

  /**
   * Event names setter.
   * @param  {Array}  eventNames - Event names
   */
  set eventNames(eventNames = []) {
    this.eventNames = this.eventNames.concat(eventNames);
  }

  /**
   * Log event
   * @param  {String} options.level - Log verbosity level.
   * @param  {String} options.event - Event name.
   * @param  {String} options.message - Event message.
   * @param  {Object} options.meta - Meta information. Contains event detail and exec duration.
   */
  log({ level = 'info', event = 'info', message = '', meta = {} }) {
    if (!_.includes(this.eventNames, event)) {
      throw new Error('Event name not registered');
    }

    logger[level](event, message, meta);
    this.emit(event, { message, meta });
  }
}

module.exports = new Event();
