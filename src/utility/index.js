/**
 * @namespace utility
 */

/**
 * {@link module:logger}
 * @memberof utility
 */
const logger = require('./logger');

/**
 * {@link module:event}
 * @memberof utility
 */
const event = require('./event');


module.exports = { logger, event };
