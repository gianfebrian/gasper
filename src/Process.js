const { Command } = require('./command');

/**
 * This class will process a report. Thant means viewing, printing or exporting.
 */
class Process {
  /**
   * Create a process command option chains.
   * @param  {object}  options - Command builder options.
   * @return {Process} self constructor.
   */
  constructor(options) {
    this.command = new Command(options).compile();
  }

  /**
   * Set input option.
   * @param  {string}  value - Input file (.jrxml|.jasper|.jrprint).
   * @return {Process} self
   */
  input(value) {
    this.command.option(value);

    return this;
  }

  /**
   * Set option, whether to write .jasper file to input directory
   * if jrxml is processed.
   * @return {Process} self.
   */
  writeJasper() {
    this.command.option('-w');

    return this;
  }

  /**
   * Set output option
   * @param  {string}  value - Directory or basename of outputfile(s).
   * @return {Process} self.
   */
  output(value) {
    this.command.option('-o', value);

    return this;
  }

  /**
   * Set output format option.
   * <pre>
   * view, print, pdf, rtf, xls,
   * xlsMeta, xlsx, docx, odt, ods, pptx,
   * csv, csvMeta, html, xhtml, xml, jrprint
   * </pre>
   * @param  {string}  value - <i>see method description</i>
   * @return {Process} self
   */
  format(value) {
    this.command.option('-f', value);

    return this;
  }

  /**
   * Set report filter option.
   * @param  {array}  value - Filter: a, ae, u, ue, p, pe (<i>see usage</i>).
   * @return {Process} self.
   */
  filter(value) {
    this.command.option('-a', value);

    return this;
  }

  /**
   * Set report parameter option.
   * @param  {string} value - Report parameter: name=value [...].
   * @return {Process} self.
   */
  param(value) {
    this.command.option('-P', value);

    return this;
  }

  /**
   * Set report resource option
   * If resource is not given the input directory is used.
   * @param  {string} value - Path to resource  dir or jar file.
   * @return {Process} self.
   */
  resource(value) {
    this.command.option('-r', value);

    return this;
  }

  /**
   * Set data source type option.
   * Datasource type:
   * <pre>
   * none, csv,  xml, mysql, postgres, oracle, generic (jdbc)
   * </pre>
   * @param  {string} value - Data source type <i>See method description</i>.
   * @return {Process} self.
   */
  dataSourceType(value) {
    this.command.option('-t', value);

    return this;
  }

  /**
   * Set database host name option.
   * @param  {string} value - Host name.
   * @return {Process} self.
   */
  databaseHost(value) {
    this.command.option('-H', value);

    return this;
  }

  /**
   * Set database port option.
   * @param  {string} value - Port number.
   * @return {Process} self.
   */
  databasePort(value) {
    this.command.option('--db-port', value);

    return this;
  }

  /**
   * Set database user option.
   * @param  {string} value - Database user.
   * @return {Process} self.
   */
  databaseUser(value) {
    this.command.option('-u', value);

    return this;
  }

  /**
   * Set database password option.
   * @param  {string} value - Database password.
   * @return {Process} self.
   */
  databasePassword(value) {
    this.command.option('-p', value);

    return this;
  }

  /**
   * Set databasse name option.
   * @param  {string} value - Database name.
   * @return {Process} self.
   */
  databaseName(value) {
    this.command.option('-n', value);

    return this;
  }

  /**
   * Set jdbc driver class name option for generic data source type.
   * (<i>See dataSourceType</i>).
   * @param  {string} value - jdbc driver class name
   * @return {Process} self.
   */
  jdbcDriverClassName(value) {
    this.command.option('--db-driver', value);

    return this;
  }

  /**
   * Set jdbc url (without user and password).
   * @param  {string} value - jdbc url.
   * @return {Process} self.
   */
  jdbcUrl(value) {
    this.command.option('--db-url', value);

    return this;
  }

  /**
   * Set jdbc directory option where jdbc driver jars are located.
   * @param  {string} value - jdbc directory path. Default <pre>./jdbc</pre>.
   * @return {Process} self.
   */
  jdbcDir(value) {
    this.command.option('--jdbc-dir', value);

    return this;
  }

  /**
   * Set sid option (for oracle).
   * @param  {string} value - Oracle sid.
   * @return {Process} self.
   */
  oracleSid(value) {
    this.command.option('--db-sid', value);

    return this;
  }

  /**
   * Set input file option for file based data source.
   * @param  {string} value - File path.
   * @return {Process} self.
   */
  dataFile(value) {
    this.command.option('--data-file', value);

    return this;
  }

  /**
   * Set csv first row option (if csv contains column headers).
   * @return {Process} self.
   */
  csvFirstRow() {
    this.command.option('--csv-first-row');

    return this;
  }

  /**
   * Set csv column list option.
   * @param  {array} value - List of column names.
   * @return {Process} self.
   */
  csvColumns(value) {
    this.command.option('--csv-columns', value);

    return this;
  }

  /**
   * Set csv row delimiter option.
   * @param  {string} value - Row delimiter. Defaults <pre>line.separator</pre>.
   * @return {Process} self.
   */
  csvRowDelimiter(value) {
    this.command.option('--csv-record-del', value);

    return this;
  }

  /**
   * Set csv column delimiter option.
   * @param  {string} value - Column delimiter. Defaults ",".
   * @return {Process} self.
   */
  csvColumnDelimiter(value) {
    this.command.option('--csv-field-del', value);

    return this;
  }

  /**
   * Set csv charset.
   * @param  {string} value - Csv charset. Defaults "utf-8".
   * @return {Process} self.
   */
  csvCharset(value) {
    this.command.option('--csv-charset', value);

    return this;
  }

  /**
   * Set XML XPATH option for XML data source
   * @param  {string} value - XML Xpath.
   * @return {Process} self.
   */
  xmlXPath(value) {
    this.command.option('--xml-xpath', value);

    return this;
  }

  /**
   * Set printer name option.
   * @param  {string} value - Printer name.
   * @return {Process} self.
   */
  printerName(value) {
    this.command.option('-N', value);

    return this;
  }

  /**
   * Set whether to show print dialog option.
   * @return {Process} self.
   */
  showPrintDialog() {
    this.command.option('-d');

    return this;
  }

  /**
   * Set internal report/document name.
   * @param {string} value - Internal report/document name.
   * @return {Process} self.
   */
  setInternalReportName(value) {
    this.command.option('-s', value);

    return this;
  }

  /**
   * Set number of print copies option.
   * @param  {number} value - Number of copies.
   * @return {Process} self.
   */
  numberOfPrintCopies(value) {
    this.command.option('-c', value);

    return this;
  }

  /**
   * Set export CSV (Metadata) column delimiter option.
   * @param  {string} value - Column delimiter. Defaults ",".
   * @return {Process} self.
   */
  exportCsvColumnDelimiter(value) {
    this.command.option('--out-field-del', value);

    return this;
  }

  /**
   * Set export CSV charset.
   * @param  {string} value - CSV charset. Defaults "utf-8".
   * @return {Process} self.
   */
  exportCharset(value) {
    this.command.option('--out-charset', value);

    return this;
  }
}

module.exports = Process;
